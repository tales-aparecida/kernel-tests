# storage/nvdimm/btt_suspend_mem_resume_io

Storage: Test suspend to mem and resume during btt io

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
